// Copyright 2015 PingCAP, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// See the License for the specific language governing permissions and
// limitations under the License.

package ast

import (
	"strconv"
	"strings"

	"gitlab.com/feloxx/cdpparser/auth"
	"gitlab.com/feloxx/cdpparser/model"
	"gitlab.com/feloxx/cdpparser/types"
)

var (
	_ DDLNode = &AlterTableStmt{}
	_ DDLNode = &CreateDatabaseStmt{}
	_ DDLNode = &CreateIndexStmt{}
	_ DDLNode = &CreateTableStmt{}
	_ DDLNode = &CreateViewStmt{}
	_ DDLNode = &DropDatabaseStmt{}
	_ DDLNode = &DropIndexStmt{}
	_ DDLNode = &DropTableStmt{}
	_ DDLNode = &RenameTableStmt{}
	_ DDLNode = &TruncateTableStmt{}

	_ Node = &AlterTableSpec{}
	_ Node = &ColumnDef{}
	_ Node = &ColumnOption{}
	_ Node = &ColumnPosition{}
	_ Node = &Constraint{}
	_ Node = &IndexColName{}
	_ Node = &ReferenceDef{}
)

// CharsetOpt is used for parsing charset option from SQL.
type CharsetOpt struct {
	Chs string
	Col string
}

/* ***********************************************************************************************************
 * DatabaseOption
 */
// DatabaseOptionType is the type for database options.
type DatabaseOptionType int

// Database option types.
const (
	DatabaseOptionNone DatabaseOptionType = iota
	DatabaseOptionCharset
	DatabaseOptionCollate
)

// DatabaseOption represents database option.
type DatabaseOption struct {
	DefaultKwdOpt string
	Tp    DatabaseOptionType
	EqOpt string
	Value string
}

func (n *DatabaseOption) Text() string {
	defaultKwdOpt := n.DefaultKwdOpt
	tp := ""
	switch n.Tp {
	case 0:
		tp = ""
	case 1:
		tp = "CHARACTER SET"
	case 2:
		tp = "COLLATE"
	}
	eqOpt := n.EqOpt
	charsetStringName := n.Value

	str := []string{
		defaultKwdOpt,
		tp,
		eqOpt,
		charsetStringName,
	}

	return strings.Join(ListFilterTrim(str), " ")
}

/* ***********************************************************************************************************
 * CreateDatabaseStmt
 */
// CreateDatabaseStmt is a statement to create a database.
// See https://dev.mysql.com/doc/refman/5.7/en/create-database.html
type CreateDatabaseStmt struct {
	ddlNode

	IfNotExists bool
	Name        string
	Options     []*DatabaseOption
}

func (n *CreateDatabaseStmt) GetDatabase() string {
	return n.Name
}

func (n *CreateDatabaseStmt) SetDatabase(text string) {
	n.Name = text
}

func (n *CreateDatabaseStmt) Text() string {
	databaseSym := "DATABASE"
	ifNotExists := ""
	if n.IfNotExists {
		ifNotExists = "IF NOT EXISTS"
	}
	dbName := n.Name
	var tempDatabaseOptionListOpt []string
	for _, value := range n.Options {
		tempDatabaseOptionListOpt = append(tempDatabaseOptionListOpt, value.Text())
	}
	databaseOptionListOpt := strings.Join(tempDatabaseOptionListOpt, " ")

	str := []string{
		"CREATE",
		databaseSym,
		ifNotExists,
		dbName,
		databaseOptionListOpt,
	}

	return strings.Join(ListFilterTrim(str), " ")
}

// Accept implements Node Accept interface.
func (n *CreateDatabaseStmt) Accept(v Visitor) (Node, bool) {
	newNode, skipChildren := v.Enter(n)
	if skipChildren {
		return v.Leave(newNode)
	}
	n = newNode.(*CreateDatabaseStmt)
	return v.Leave(n)
}

/* ***********************************************************************************************************
 * DropDatabaseStmt
 */
// DropDatabaseStmt is a statement to drop a database and all tables in the database.
// See https://dev.mysql.com/doc/refman/5.7/en/drop-database.html
type DropDatabaseStmt struct {
	ddlNode

	IfExists bool
	Name     string
}

func (n *DropDatabaseStmt) SetDatabase (text string)  {
  n.Name = text
}

func (n *DropDatabaseStmt) GetDatabase() string  {
	return n.Name
}

func (n *DropDatabaseStmt) Text() string {
	databaseSym := "DATABASE"
	ifExists := ""
	if n.IfExists {
		ifExists = "IF EXISTS"
	}
	dbName := n.Name

	str := []string{
		"DROP",
		databaseSym,
		ifExists,
		dbName,
	}

	return strings.Join(ListFilterTrim(str), " ")
}

// Accept implements Node Accept interface.
func (n *DropDatabaseStmt) Accept(v Visitor) (Node, bool) {
	newNode, skipChildren := v.Enter(n)
	if skipChildren {
		return v.Leave(newNode)
	}
	n = newNode.(*DropDatabaseStmt)
	return v.Leave(n)
}

/* ***********************************************************************************************************
 * IndexColName 
 */
// IndexColName is used for parsing index column name from SQL.
type IndexColName struct {
	node

	Column *ColumnName
	Length int
}

func (n *IndexColName) Text() string {
	return n.Column.String()
}

// Accept implements Node Accept interface.
func (n *IndexColName) Accept(v Visitor) (Node, bool) {
	newNode, skipChildren := v.Enter(n)
	if skipChildren {
		return v.Leave(newNode)
	}
	n = newNode.(*IndexColName)
	node, ok := n.Column.Accept(v)
	if !ok {
		return n, false
	}
	n.Column = node.(*ColumnName)
	return v.Leave(n)
}

/* ***********************************************************************************************************
 * ReferenceDef
 */
// ReferenceDef is used for parsing foreign key reference option from SQL.
// See http://dev.mysql.com/doc/refman/5.7/en/create-table-foreign-keys.html
type ReferenceDef struct {
	node

	Table         *TableName
	IndexColNames []*IndexColName
	OnDelete      *OnDeleteOpt
	OnUpdate      *OnUpdateOpt
}

// 外层要做判空处理
func (n *ReferenceDef) Text() string {
	table := n.Table.Text()
	var tempIndexColNames []string
	for _, value := range n.IndexColNames {
		tempIndexColNames = append(tempIndexColNames, value.Text())
	}
	indexColNames := "(" + strings.Join(ListFilterTrim(tempIndexColNames), ",") + ")"
	onDelete := n.OnDelete.Text()
	onUpdate := n.OnUpdate.Text()

	str := []string{
		"REFERENCES",
		table,
		indexColNames,
		onDelete,
		onUpdate,
	}

	return strings.Join(ListFilterTrim(str), " ")
}

// Accept implements Node Accept interface.
func (n *ReferenceDef) Accept(v Visitor) (Node, bool) {
	newNode, skipChildren := v.Enter(n)
	if skipChildren {
		return v.Leave(newNode)
	}
	n = newNode.(*ReferenceDef)
	node, ok := n.Table.Accept(v)
	if !ok {
		return n, false
	}
	n.Table = node.(*TableName)
	for i, val := range n.IndexColNames {
		node, ok = val.Accept(v)
		if !ok {
			return n, false
		}
		n.IndexColNames[i] = node.(*IndexColName)
	}
	onDelete, ok := n.OnDelete.Accept(v)
	if !ok {
		return n, false
	}
	n.OnDelete = onDelete.(*OnDeleteOpt)
	onUpdate, ok := n.OnUpdate.Accept(v)
	if !ok {
		return n, false
	}
	n.OnUpdate = onUpdate.(*OnUpdateOpt)
	return v.Leave(n)
}

// ReferOptionType is the type for refer options.
type ReferOptionType int

// Refer option types.
const (
	ReferOptionNoOption ReferOptionType = iota
	ReferOptionRestrict
	ReferOptionCascade
	ReferOptionSetNull
	ReferOptionNoAction
)

// String implements fmt.Stringer interface.
func (r ReferOptionType) String() string {
	switch r {
	case ReferOptionRestrict:
		return "RESTRICT"
	case ReferOptionCascade:
		return "CASCADE"
	case ReferOptionSetNull:
		return "SET NULL"
	case ReferOptionNoAction:
		return "NO ACTION"
	}
	return ""
}

// OnDeleteOpt is used for optional on delete clause.
type OnDeleteOpt struct {
	node
	ReferOpt ReferOptionType
}

// Accept implements Node Accept interface.
func (n *OnDeleteOpt) Accept(v Visitor) (Node, bool) {
	newNode, skipChildren := v.Enter(n)
	if skipChildren {
		return v.Leave(newNode)
	}
	n = newNode.(*OnDeleteOpt)
	return v.Leave(n)
}

// OnUpdateOpt is used for optional on update clause.
type OnUpdateOpt struct {
	node
	ReferOpt ReferOptionType
}

// Accept implements Node Accept interface.
func (n *OnUpdateOpt) Accept(v Visitor) (Node, bool) {
	newNode, skipChildren := v.Enter(n)
	if skipChildren {
		return v.Leave(newNode)
	}
	n = newNode.(*OnUpdateOpt)
	return v.Leave(n)
}

/* ***********************************************************************************************************
 * ColumnOption
 */
// ColumnOptionType is the type for ColumnOption.
type ColumnOptionType int

// ColumnOption types.
const (
	ColumnOptionNoOption ColumnOptionType = iota
	ColumnOptionPrimaryKey
	ColumnOptionNotNull
	ColumnOptionAutoIncrement
	ColumnOptionDefaultValue
	ColumnOptionUniqKey
	ColumnOptionNull
	ColumnOptionOnUpdate // For Timestamp and Datetime only.
	ColumnOptionFulltext
	ColumnOptionComment
	ColumnOptionGenerated
	ColumnOptionReference
)

// ColumnOption is used for parsing column constraint info from SQL.
type ColumnOption struct {
	node

	Tp ColumnOptionType
	// Expr is used for ColumnOptionDefaultValue/ColumnOptionOnUpdateColumnOptionGenerated.
	// For ColumnOptionDefaultValue or ColumnOptionOnUpdate, it's the target value.
	// For ColumnOptionGenerated, it's the target expression.
	Expr ExprNode
	// Stored is only for ColumnOptionGenerated, default is false.
	Stored bool
	// Refer is used for foreign key.
	Refer *ReferenceDef

	Value string
}

// 外层做判空处理
func (n *ColumnOption) Text() string {
	tp := ""
	switch n.Tp {
	case 0:
		tp = ""
	case 1:
		tp = "PRIMARY KEY" //ColumnOptionPrimaryKey
	case 2:
		tp = "NOT NULL" //ColumnOptionNotNull
	case 3:
		tp = "AUTO_INCREMENT" //ColumnOptionAutoIncrement
	case 4:
		tp = "DEFAULT" //ColumnOptionDefaultValue
	case 5:
		tp = "UNIQUE" //ColumnOptionUniqKey
	case 6:
		tp = "NULL" //ColumnOptionNull
	case 7:
		tp = "ON UPDATE" //ColumnOptionOnUpdate
	case 8:
		tp = "FULLTEXT" //ColumnOptionFulltext
	case 9:
		tp = "COMMENT" //ColumnOptionComment
	case 10:
		tp = "GENERATED ALWAYS" //ColumnOptionGenerated
	case 11:
		tp = "REFERENCES" //ColumnOptionReference
		//case 12: tp = "COLLATE"          //ColumnOptionCollate
	}
	stored := "" //todo 这2个字段还不知道怎么操作
	refer := ""
	if n.Refer != nil {
		refer = n.Refer.Text()
	}
	v := n.Value
	//todo 默认值字段 这里有两种类型 默认值 和还不知道啥, 还有默认值怎么加双引号
	expr := ""
	switch n.Expr.(type) {
	case *ValueExpr:
		valueExpr := n.Expr.(*ValueExpr)
		expr, _ = valueExpr.ToString()
	case *FuncCallExpr:
		funcCallExpr := n.Expr.(*FuncCallExpr)
		expr = funcCallExpr.Text()
	}

	str := []string{
		tp,
		expr,
		stored,
		refer,
		v,
	}

	return strings.Join(ListFilterTrim(str), " ")
}

// Accept implements Node Accept interface.
func (n *ColumnOption) Accept(v Visitor) (Node, bool) {
	newNode, skipChildren := v.Enter(n)
	if skipChildren {
		return v.Leave(newNode)
	}
	n = newNode.(*ColumnOption)
	if n.Expr != nil {
		node, ok := n.Expr.Accept(v)
		if !ok {
			return n, false
		}
		n.Expr = node.(ExprNode)
	}
	return v.Leave(n)
}

/* ***********************************************************************************************************
 * IndexOption
 */
// IndexOption is the index options.
//    KEY_BLOCK_SIZE [=] value
//  | index_type
//  | WITH PARSER parser_name
//  | COMMENT 'string'
// See http://dev.mysql.com/doc/refman/5.7/en/create-table.html
type IndexOption struct {
	node

	KeyBlockSize uint64
	EqOpt		     string
	Tp           model.IndexType
	Comment      string
}

// 外层判空处理
func (n *IndexOption) Text() string {
	eqOpt := n.EqOpt
	keyBlockSize := ""
	if n.KeyBlockSize > 1 {
		keyBlockSize += "KEY_BLOCK_SIZE" + eqOpt + strconv.FormatUint(n.KeyBlockSize, 10)
	}
	tp := ""
	switch n.Tp {
	case model.IndexTypeHash:
		tp = "USING HASH"
	case model.IndexTypeBtree:
		tp = "USING BTREE"
	}
	comment := ""
	if n.Comment != "" {
		comment = "COMMENT " + "'" + n.Comment + "'"
	}

	str := []string{
		keyBlockSize,
		tp,
		comment,
	}

	return strings.Join(ListFilterTrim(str), " ")
}

// Accept implements Node Accept interface.
func (n *IndexOption) Accept(v Visitor) (Node, bool) {
	newNode, skipChildren := v.Enter(n)
	if skipChildren {
		return v.Leave(newNode)
	}
	n = newNode.(*IndexOption)
	return v.Leave(n)
}

/* ***********************************************************************************************************
 * Constraint 
 */
// ConstraintType is the type for Constraint.
type ConstraintType int

// ConstraintTypes
const (
	ConstraintNoConstraint ConstraintType = iota
	ConstraintPrimaryKey
	ConstraintKey
	ConstraintIndex
	ConstraintUniq
	ConstraintUniqKey
	ConstraintUniqIndex
	ConstraintForeignKey
	ConstraintFulltext
)

// Constraint is constraint for table definition.
type Constraint struct {
	node

	Tp   ConstraintType
	Name string

	Keys []*IndexColName // Used for PRIMARY KEY, UNIQUE, ......

	Refer *ReferenceDef // Used for foreign key.

	Option *IndexOption // Index Options
}

// 外层判空处理
func (n *Constraint) Text() string {
	tp := ""
	switch n.Tp {
		case 0: tp = ""
		case 1: tp = "PRIMARY KEY"
		case 2: tp = "KEY"
		case 3: tp = "INDEX"
		case 4: tp = "UNIQUE"
		case 5: tp = "UNIQUE KEY"
		case 6: tp = "UNIQUE INDEX"
		case 7: tp = "FOREIGN KEY"
	  case 8: tp = "FULLTEXT INDEX"
	}

	name := n.Name

	var tempKeys []string
	for _, value := range n.Keys {
		tempKeys = append(tempKeys, value.Text())
	}
	keys := "(" + strings.Join(ListFilterTrim(tempKeys), ",") + ")"

	refer := ""
	if n.Refer != nil {
		refer = n.Refer.Text()
	}
	op := ""
	if n.Option != nil {
		op = n.Option.Text()
	}

	str := []string{
		tp,
		name,
		keys,
		refer,
		op,
	}

	return strings.Join(ListFilterTrim(str), " ")
}

// Accept implements Node Accept interface.
func (n *Constraint) Accept(v Visitor) (Node, bool) {
	newNode, skipChildren := v.Enter(n)
	if skipChildren {
		return v.Leave(newNode)
	}
	n = newNode.(*Constraint)
	for i, val := range n.Keys {
		node, ok := val.Accept(v)
		if !ok {
			return n, false
		}
		n.Keys[i] = node.(*IndexColName)
	}
	if n.Refer != nil {
		node, ok := n.Refer.Accept(v)
		if !ok {
			return n, false
		}
		n.Refer = node.(*ReferenceDef)
	}
	if n.Option != nil {
		node, ok := n.Option.Accept(v)
		if !ok {
			return n, false
		}
		n.Option = node.(*IndexOption)
	}
	return v.Leave(n)
}

/* ***********************************************************************************************************
 * ColumnDef
 */
// ColumnDef is used for parsing column definition from SQL.
type ColumnDef struct {
	node

	Name    *ColumnName
	Tp      *types.FieldType
	Options []*ColumnOption
}

func (n *ColumnDef) GetName() string {
	return n.Name.OrigColName()
}
func (n *ColumnDef) GetLen() int {
	return n.Tp.Flen
}
func (n *ColumnDef) GetTp() string {
	return n.Tp.String()
}

// 外层判空处理
func (n *ColumnDef) Text() string {
	name := n.Name.OrigColName()
	tp := n.Tp.String() //todo 这个地方可能为空
	var tempOp []string
	for _, value := range n.Options {
		tempOp = append(tempOp, value.Text())
	}
	op := strings.Join(ListFilterTrim(tempOp), " ")

	str := []string{
		name,
		tp,
		op,
	}

	return strings.Join(ListFilterTrim(str), " ")
}

// Accept implements Node Accept interface.
func (n *ColumnDef) Accept(v Visitor) (Node, bool) {
	newNode, skipChildren := v.Enter(n)
	if skipChildren {
		return v.Leave(newNode)
	}
	n = newNode.(*ColumnDef)
	node, ok := n.Name.Accept(v)
	if !ok {
		return n, false
	}
	n.Name = node.(*ColumnName)
	for i, val := range n.Options {
		node, ok := val.Accept(v)
		if !ok {
			return n, false
		}
		n.Options[i] = node.(*ColumnOption)
	}
	return v.Leave(n)
}

/* ***********************************************************************************************************
 * CreateTableStmt
 */
// CreateTableStmt is a statement to create a table.
// See https://dev.mysql.com/doc/refman/5.7/en/create-table.html
type CreateTableStmt struct {
	ddlNode

	IfNotExists bool
	Table       *TableName
	ReferTable  *TableName
	Cols        []*ColumnDef
	Constraints []*Constraint
	Options     []*TableOption
	Partition   *PartitionOptions
	OnDuplicate OnDuplicateCreateTableSelectType
	Select      ResultSetNode
}

func (n *CreateTableStmt) SetIfNotExists(b bool) {
  n.IfNotExists = b
}
func (n *CreateTableStmt) GetIfNotExists() bool {
	return n.IfNotExists
}

func (n *CreateTableStmt) SetDataBase(db string) {
	n.Table.Schema.O = db
}
func (n *CreateTableStmt) GetDataBase() string {
	return n.Table.Schema.String()
}

func (n *CreateTableStmt) SetTable(table string) {
	n.Table.Name.O = table
}
func (n *CreateTableStmt) GetTable() string {
	return n.Table.Name.String()
}

func (n *CreateTableStmt) Text() string {
	ifNotExists := ""
	if n.IfNotExists {
		ifNotExists = "IF NOT EXISTS"
	}
	table := n.Table.Text()
	referTable := ""
	if n.ReferTable != nil {
		referTable = n.ReferTable.Text()
	}

	var tempTableElementListConstraints []string
	for _, value := range n.Cols {
		tempTableElementListConstraints = append(tempTableElementListConstraints, value.Text())
	}
	for _, value := range n.Constraints {
		tempTableElementListConstraints = append(tempTableElementListConstraints, value.Text())
	}
	tableElementListConstraints := "(" + strings.Join(ListFilterTrim(tempTableElementListConstraints), ",") + ")"

	var tempCreateTableOptionListOpt []string
	for _, value := range n.Options {
		tempCreateTableOptionListOpt = append(tempCreateTableOptionListOpt, value.Text())
	}
	createTableOptionListOpt := strings.Join(tempCreateTableOptionListOpt, " ")

	partitionOpt := ""
	if n.Partition != nil {
		n.Partition.Text()
	}

	var str []string
	if referTable == "" {
		str = []string{
			"CREATE", "TABLE",
			ifNotExists,
			table,
			tableElementListConstraints,
			createTableOptionListOpt,
			partitionOpt,
		}

	} else {
		str = []string{
			"CREATE", "TABLE",
			ifNotExists,
			table, "LIKE", referTable,
		}
	}

	return strings.Join(ListFilterTrim(str), " ")
}

// Accept implements Node Accept interface.
func (n *CreateTableStmt) Accept(v Visitor) (Node, bool) {
	newNode, skipChildren := v.Enter(n)
	if skipChildren {
		return v.Leave(newNode)
	}
	n = newNode.(*CreateTableStmt)
	node, ok := n.Table.Accept(v)
	if !ok {
		return n, false
	}
	n.Table = node.(*TableName)
	if n.ReferTable != nil {
		node, ok = n.ReferTable.Accept(v)
		if !ok {
			return n, false
		}
		n.ReferTable = node.(*TableName)
	}
	for i, val := range n.Cols {
		node, ok = val.Accept(v)
		if !ok {
			return n, false
		}
		n.Cols[i] = node.(*ColumnDef)
	}
	for i, val := range n.Constraints {
		node, ok = val.Accept(v)
		if !ok {
			return n, false
		}
		n.Constraints[i] = node.(*Constraint)
	}
	if n.Select != nil {
		node, ok := n.Select.Accept(v)
		if !ok {
			return n, false
		}
		n.Select = node.(ResultSetNode)
	}

	return v.Leave(n)
}

/* ***********************************************************************************************************
 * DropTableStmt
 */
// DropTableStmt is a statement to drop one or more tables.
// See https://dev.mysql.com/doc/refman/5.7/en/drop-table.html
type DropTableStmt struct {
	ddlNode

	TempOraryOpt string
	IfExists bool
	Tables   []*TableName
	IsView   bool
	RestrictOrCascadeOpt string
}

func (n *DropTableStmt) SetIfExists(b bool) {
	n.IfExists = b
}
func (n *DropTableStmt) GetIfExists() bool {
	return n.IfExists
}

// 目前只支持对第一个表的操作, 暂不支持多表操作
func (n *DropTableStmt) SetDataBase(text string) {
	n.Tables[0].SetDataBase(text)
}
func (n *DropTableStmt) GetDataBase() string {
	return n.Tables[0].GetDataBase()
}

func (n *DropTableStmt) SetTable(text string) {
	n.Tables[0].SetTable(text)
}
func (n *DropTableStmt) GetTable() string {
	return n.Tables[0].GetTable()
}

func (n *DropTableStmt) Text() string {
	tempOraryOpt := n.TempOraryOpt
	ifExists := ""
	if n.IfExists {
		ifExists = "IF EXISTS"
	}
	var tempTable []string
	for _, value := range n.Tables {
		tempTable = append(tempTable, value.Text())
	}
	tables := strings.Join(tempTable, ",")

	restrictOrCascadeOpt := n.RestrictOrCascadeOpt

	str := []string{
		"DROP",
		tempOraryOpt,
		"TABLE",
		ifExists,
		tables,
		restrictOrCascadeOpt,
	}

	return strings.Join(ListFilterTrim(str), " ")
}

// Accept implements Node Accept interface.
func (n *DropTableStmt) Accept(v Visitor) (Node, bool) {
	newNode, skipChildren := v.Enter(n)
	if skipChildren {
		return v.Leave(newNode)
	}
	n = newNode.(*DropTableStmt)
	for i, val := range n.Tables {
		node, ok := val.Accept(v)
		if !ok {
			return n, false
		}
		n.Tables[i] = node.(*TableName)
	}
	return v.Leave(n)
}

/* ***********************************************************************************************************
 * RenameTableStmt
 */
// RenameTableStmt is a statement to rename a table.
// See http://dev.mysql.com/doc/refman/5.7/en/rename-table.html
type RenameTableStmt struct {
	ddlNode

	OldTable *TableName
	NewTable *TableName

	// TableToTables is only useful for syncer which depends heavily on tidb parser to do some dirty work for now.
	// TODO: Refactor this when you are going to add full support for multiple schema changes.
	TableToTables []*TableToTable
}

func (n *RenameTableStmt) Text() string {
	var tempTableToTable []string
	for _, value := range n.TableToTables {
		tempTableToTable = append(tempTableToTable, value.Text())
	}
	table2table := strings.Join(tempTableToTable, ",")

	str := []string{
		"RENAME", "TABLE",
		table2table,
	}

	return strings.Join(ListFilterTrim(str), " ")
}

// Accept implements Node Accept interface.
func (n *RenameTableStmt) Accept(v Visitor) (Node, bool) {
	newNode, skipChildren := v.Enter(n)
	if skipChildren {
		return v.Leave(newNode)
	}
	n = newNode.(*RenameTableStmt)
	node, ok := n.OldTable.Accept(v)
	if !ok {
		return n, false
	}
	n.OldTable = node.(*TableName)
	node, ok = n.NewTable.Accept(v)
	if !ok {
		return n, false
	}
	n.NewTable = node.(*TableName)

	for i, t := range n.TableToTables {
		node, ok := t.Accept(v)
		if !ok {
			return n, false
		}
		n.TableToTables[i] = node.(*TableToTable)
	}

	return v.Leave(n)
}

/* ***********************************************************************************************************
 * TableToTable
 */
// TableToTable represents renaming old table to new table used in RenameTableStmt.
type TableToTable struct {
	node
	OldTable *TableName
	NewTable *TableName
}

func (n *TableToTable) Text() string {
	return n.OldTable.Text() + " TO " + n.NewTable.Text()
}

// Accept implements Node Accept interface.
func (n *TableToTable) Accept(v Visitor) (Node, bool) {
	newNode, skipChildren := v.Enter(n)
	if skipChildren {
		return v.Leave(newNode)
	}
	n = newNode.(*TableToTable)
	node, ok := n.OldTable.Accept(v)
	if !ok {
		return n, false
	}
	n.OldTable = node.(*TableName)
	node, ok = n.NewTable.Accept(v)
	if !ok {
		return n, false
	}
	n.NewTable = node.(*TableName)
	return v.Leave(n)
}

/* ***********************************************************************************************************
 * CreateViewStmt
 */
// CreateViewStmt is a statement to create a View.
// See https://dev.mysql.com/doc/refman/5.7/en/create-view.html
type CreateViewStmt struct {
	ddlNode

	OrReplace   bool
	ViewName    *TableName
	Cols        []model.CIStr
	Select      StmtNode
	Algorithm   model.ViewAlgorithm
	Definer     *auth.UserIdentity
	Security    model.ViewSecurity
	CheckOption model.ViewCheckOption
}

func (n *CreateViewStmt) SetViewName(text string) {
	n.ViewName.SetText(text)
}

func (n *CreateViewStmt) Text() string {
	orReplace := ""
	if n.OrReplace {
		orReplace = "OR REPLACE"
	}
	viewAlgorithm := n.Algorithm.String()
	viewDefiner := n.CheckOption.String()
	viewSQLSecurity := n.Security.String()
	viewName := n.ViewName.Text()
	var tempViewFieldList []string
	for _, value := range n.Cols {
		tempViewFieldList = append(tempViewFieldList, value.String())
	}
	viewFieldList := strings.Join(tempViewFieldList, ",")
	sel := n.Select.Text()

	str := []string{
		"CREATE",
		orReplace,
		viewAlgorithm,
		viewDefiner,
		viewSQLSecurity,
		"VIEW",
		viewName,
		viewFieldList,
		"AS",
		sel,
	}

	return strings.Join(ListFilterTrim(str), " ")
}

// Accept implements Node Accept interface.
func (n *CreateViewStmt) Accept(v Visitor) (Node, bool) {
	newNode, skipChildren := v.Enter(n)
	if skipChildren {
		return v.Leave(newNode)
	}
	n = newNode.(*CreateViewStmt)
	node, ok := n.ViewName.Accept(v)
	if !ok {
		return n, false
	}
	n.ViewName = node.(*TableName)
	selnode, ok := n.Select.Accept(v)
	if !ok {
		return n, false
	}
	n.Select = selnode.(*SelectStmt)
	return v.Leave(n)
}

/* ***********************************************************************************************************
 * CreateIndexStmt
 */
// CreateIndexStmt is a statement to create an index.
// See https://dev.mysql.com/doc/refman/5.7/en/create-index.html
type CreateIndexStmt struct {
	ddlNode

	IndexName     string
	Table         *TableName
	Unique        bool
	IndexColNames []*IndexColName
	IndexOption   *IndexOption
}

func (n *CreateIndexStmt) GetDataBase() string {
	return n.Table.GetDataBase()
}
func (n *CreateIndexStmt) SetDataBase(text string) {
	n.Table.SetDataBase(text)
}

func (n *CreateIndexStmt) GetTable() string {
  return n.Table.Name.O
}
func (n *CreateIndexStmt) SetTable(text string) {
  n.Table.Name.O = text
}

func (n *CreateIndexStmt) Text() string {
	createIndexStmtUnique := ""
	if n.Unique {
		createIndexStmtUnique = "UNIQUE"
	}
	identifier := n.IndexName
	tableName := n.Table.Text()
	var tempIndexColNameList []string
	for _, value := range n.IndexColNames {
		tempIndexColNameList = append(tempIndexColNameList, value.Text())
	}
	indexColNameList := "(" + strings.Join(tempIndexColNameList, ",") + ")"
	//var tempIndexOptionList []string
	//for _, value := range n.IndexOptions {
	//	tempIndexOptionList = append(tempIndexOptionList, value.Text())
	//}
	//indexOptionList := strings.Join(tempIndexOptionList, " ")
	
	indexOption := ""
	if n.IndexOption != nil {
		indexOption = n.IndexOption.Text()
	}

	str := []string{
		"CREATE",
		createIndexStmtUnique,
		"INDEX",
		identifier,
		"ON",
		tableName,
		indexColNameList,
		//indexOptionList,
		indexOption,
	}

	return strings.Join(ListFilterTrim(str), " ")
}

// Accept implements Node Accept interface.
func (n *CreateIndexStmt) Accept(v Visitor) (Node, bool) {
	newNode, skipChildren := v.Enter(n)
	if skipChildren {
		return v.Leave(newNode)
	}
	n = newNode.(*CreateIndexStmt)
	node, ok := n.Table.Accept(v)
	if !ok {
		return n, false
	}
	n.Table = node.(*TableName)
	for i, val := range n.IndexColNames {
		node, ok = val.Accept(v)
		if !ok {
			return n, false
		}
		n.IndexColNames[i] = node.(*IndexColName)
	}
	if n.IndexOption != nil {
		node, ok := n.IndexOption.Accept(v)
		if !ok {
			return n, false
		}
		n.IndexOption = node.(*IndexOption)
	}
	return v.Leave(n)
}

/* ***********************************************************************************************************
 * DropIndexStmt 
 */
// DropIndexStmt is a statement to drop the index.
// See https://dev.mysql.com/doc/refman/5.7/en/drop-index.html
type DropIndexStmt struct {
	ddlNode

	IfExists  bool
	IndexName string
	Table     *TableName
}

func (n *DropIndexStmt) GetDataBase() string {
	return n.Table.GetDataBase()
}
func (n *DropIndexStmt) SetDataBase(text string) {
	n.Table.SetDataBase(text)
}

func (n *DropIndexStmt) GetTable() string {
	return n.Table.Name.O
}
func (n *DropIndexStmt) SetTable(text string) {
	n.Table.Name.O = text
}

func (n *DropIndexStmt) Text() string {
	ifExists := ""
	if n.IfExists {
		ifExists += "IF EXIST"
	}
	identifier := n.IndexName
	tableName := n.Table.Text()

	result := []string{
		"DROP",
		"INDEX",
		ifExists,
		identifier,
		"ON",
		tableName,
	}

	return strings.Join(ListFilterTrim(result), " ")
}

// Accept implements Node Accept interface.
func (n *DropIndexStmt) Accept(v Visitor) (Node, bool) {
	newNode, skipChildren := v.Enter(n)
	if skipChildren {
		return v.Leave(newNode)
	}
	n = newNode.(*DropIndexStmt)
	node, ok := n.Table.Accept(v)
	if !ok {
		return n, false
	}
	n.Table = node.(*TableName)
	return v.Leave(n)
}

/* ***********************************************************************************************************
 * TableOption
 */
// TableOptionType is the type for TableOption
type TableOptionType int

// TableOption types.
const (
	TableOptionNone TableOptionType = iota
	TableOptionEngine
	TableOptionCharset
	TableOptionCollate
	TableOptionAutoIncrement
	TableOptionComment
	TableOptionAvgRowLength
	TableOptionCheckSum
	TableOptionCompression
	TableOptionConnection
	TableOptionPassword
	TableOptionKeyBlockSize
	TableOptionMaxRows
	TableOptionMinRows
	TableOptionDelayKeyWrite
	TableOptionRowFormat
	TableOptionStatsPersistent
	TableOptionShardRowID
	TableOptionPackKeys
)

// RowFormat types
const (
	RowFormatDefault uint64 = iota + 1
	RowFormatDynamic
	RowFormatFixed
	RowFormatCompressed
	RowFormatRedundant
	RowFormatCompact
)

// OnDuplicateCreateTableSelectType is the option that handle unique key values in 'CREATE TABLE ... SELECT'.
// See https://dev.mysql.com/doc/refman/5.7/en/create-table-select.html
type OnDuplicateCreateTableSelectType int

// OnDuplicateCreateTableSelect types
const (
	OnDuplicateCreateTableSelectError OnDuplicateCreateTableSelectType = iota
	OnDuplicateCreateTableSelectIgnore
	OnDuplicateCreateTableSelectReplace
)

// TableOption is used for parsing table option from SQL.
type TableOption struct {
	DefaultKwdOpt string
	Tp        TableOptionType
	EqOpt     string
	StrValue  string
	UintValue uint64
}

func (n *TableOption) Text() string {
	defaultKwdOpt := n.DefaultKwdOpt
	tp := ""
	switch n.Tp {
	case 0:
		tp = ""
	case 1:
		tp = "ENGINE" //TableOptionEngine
	case 2:
		tp = "CHARACTER SET" //TableOptionCharset
	case 3:
		tp = "DEFAULT COLLATE" //TableOptionCollate
	case 4:
		tp = "AUTO_INCREMENT" //TableOptionAutoIncrement
	case 5:
		tp = "COMMENT" //TableOptionComment
	case 6:
		tp = "AVG_ROW_LENGTH" //TableOptionAvgRowLength
	case 7:
		tp = "CHECKSUM" //TableOptionCheckSum
	case 8:
		tp = "COMPRESSION" //TableOptionCompression
	case 9:
		tp = "CONNECTION" //TableOptionConnection
	case 10:
		tp = "PASSWORD" //TableOptionPassword
	case 11:
		tp = "KEY_BLOCK_SIZE" //TableOptionKeyBlockSize
	case 12:
		tp = "MAX_ROWS" //TableOptionMaxRows
	case 13:
		tp = "MIN_ROWS" //TableOptionMinRows
	case 14:
		tp = "DELAY_KEY_WRITE" //TableOptionDelayKeyWrite
	case 15:
		tp = "ROW_FORMAT" //TableOptionRowFormat
	case 16:
		tp = "STATS_PERSISTENT" //TableOptionStatsPersistent
	case 17:
		tp = "SHARD_ROW_ID_BITS" //TableOptionShardRowID
	case 18:
		tp = "PACK_KEYS" //TableOptionPackKeyscase
	}
	eqOpt := n.EqOpt
	value := ""
	switch n.Tp {
	case 1: //engine
		if n.StrValue != "" {
			value = n.StrValue
		} else {
			value = "''"
		}
	case 4, 15: // 数值类型的结果
		value = strconv.FormatUint(n.UintValue, 10)
	case 5, 8, 9, 10: //字符类型的结果, 搞了单引号包了一下
		value = "'" + n.StrValue + "'"
	case 16: //STATS_PERSISTENT
		if n.StrValue != "" && n.StrValue == "DEFAULT" {
			value = n.StrValue
		} else {
			value = strconv.FormatUint(n.UintValue, 10)
		}
	default:
		value = n.StrValue
	}

	str := []string{
		defaultKwdOpt,
		tp,
		eqOpt,
		value,
	}

	return strings.Join(ListFilterTrim(str), " ")
}

/* ***********************************************************************************************************
 * ColumnPosition
 */
// ColumnPositionType is the type for ColumnPosition.
type ColumnPositionType int

// ColumnPosition Types
const (
	ColumnPositionNone ColumnPositionType = iota
	ColumnPositionFirst
	ColumnPositionAfter
)

// ColumnPosition represent the position of the newly added column
type ColumnPosition struct {
	node
	// Tp is either ColumnPositionNone, ColumnPositionFirst or ColumnPositionAfter.
	Tp ColumnPositionType
	// RelativeColumn is the column the newly added column after if type is ColumnPositionAfter
	RelativeColumn *ColumnName
}

// 外层要做判空处理
func (n *ColumnPosition) Text() string {
	tp := ""
	switch n.Tp {
	case 0:
		tp = ""
	case 1:
		tp = "FIRST" //ColumnPositionFirst
	case 2:
		tp = "AFTER" //ColumnPositionAftercaase
	}
	relativeColumn := n.RelativeColumn.OrigColName()

	str := []string{
		tp,
		relativeColumn,
	}

	return strings.Join(ListFilterTrim(str), " ")
}

// Accept implements Node Accept interface.
func (n *ColumnPosition) Accept(v Visitor) (Node, bool) {
	newNode, skipChildren := v.Enter(n)
	if skipChildren {
		return v.Leave(newNode)
	}
	n = newNode.(*ColumnPosition)
	if n.RelativeColumn != nil {
		node, ok := n.RelativeColumn.Accept(v)
		if !ok {
			return n, false
		}
		n.RelativeColumn = node.(*ColumnName)
	}
	return v.Leave(n)
}

/* ***********************************************************************************************************
 * AlterTable
 */
// AlterTableType is the type for AlterTableSpec.
type AlterTableType int

// AlterTable types.
const (
	AlterTableOption AlterTableType = iota + 1
	AlterTableAddColumns
	AlterTableAddConstraint
	AlterTableDropColumn
	AlterTableDropPrimaryKey
	AlterTableDropIndex
	AlterTableDropForeignKey
	AlterTableModifyColumn
	AlterTableChangeColumn
	AlterTableRenameTable
	AlterTableAlterColumn
	AlterTableLock
	AlterTableAlgorithm
	AlterTableRenameIndex
	AlterTableForce
	AlterTableAddPartitions
	AlterTableCoalescePartitions
	AlterTableDropPartition
	AlterTableTruncatePartition

	// TODO: Add more actions
)

// LockType is the type for AlterTableSpec.
// See https://dev.mysql.com/doc/refman/5.7/en/alter-table.html#alter-table-concurrency
type LockType byte

// Lock Types.
const (
	LockTypeNone LockType = iota + 1
	LockTypeDefault
	LockTypeShared
	LockTypeExclusive
)

// AlterTableSpec represents alter table specification.
type AlterTableSpec struct {
	node

	Tp              AlterTableType
	Name            string
	Constraint      *Constraint
	Options         []*TableOption
	NewTable        *TableName
	NewColumns      []*ColumnDef
	OldColumnName   *ColumnName
	Position        *ColumnPosition
	LockType        LockType
	Comment         string
	FromKey         model.CIStr
	ToKey           model.CIStr
	PartDefinitions []*PartitionDefinition
	Num             uint64
	EqOpt         string
	NewColumnsParentheses bool
}

func (n *AlterTableSpec) Text() string {
	tp := n.GetAlterType()

	name := n.Name

	constraint := ""
	if n.Constraint != nil {
		constraint = n.Constraint.Text()
	}

	var tempOp []string
	for _, value := range n.Options {
		tempOp = append(tempOp, value.Text())
	}
	op := strings.Join(ListFilterTrim(tempOp), ",")

	newTable := ""
	if n.NewTable != nil {
		newTable = n.NewTable.Text()
	}

	var tempNewCol []string
	for _, value := range n.NewColumns {
		tempNewCol = append(tempNewCol, value.Text())
	}
	newCol := ""
	if n.NewColumnsParentheses {
		newCol = "(" + strings.Join(ListFilterTrim(tempNewCol), ",") + ")" //todo 字段这个部分该怎么加括号呢
	} else {
		newCol = strings.Join(ListFilterTrim(tempNewCol), ",")
	}

	oldCol := ""
	if n.OldColumnName != nil {
		oldCol = n.OldColumnName.OrigColName()
	}

	position := ""
	if n.Position != nil && n.Position.RelativeColumn != nil{
		position = n.Position.Text()
	}

	lockType := ""
	switch n.LockType {
	case 1:
		tp = "NONE" //LockTypeNone
	case 2:
		tp = "DEFAULT" //LockTypeDefault
	case 3:
		tp = "SHARED" //LockTypeShared
	case 4:
		tp = "EXCLUSIVE" //LockTypeExclusive
	}

	eqOpt := n.EqOpt

	comment := n.Comment

	str := []string{
		tp,
		name,
		constraint,
		op,
		newTable,
		oldCol,
		newCol,
		position,
		lockType,
		eqOpt,
		comment,
	}

	return strings.Join(ListFilterTrim(str), " ")
}

func (n *AlterTableSpec) GetAlterType() string {
	var tp string
	switch n.Tp {
	case 1:
		tp = ""
	case 2:
		tp = "ADD COLUMN" //AlterTableAddColumns
	case 3:
		tp = "ADD" //AlterTableAddConstraint
	case 4:
		tp = "DROP COLUMN" //AlterTableDropColumn 要不要带column呢
	case 5:
		tp = "DROP PRIMARY KEY" //AlterTableDropPrimaryKey
	case 6:
		tp = "DROP INDEX" //AlterTableDropIndex
	case 7:
		tp = "DROP FOREIGN KEY" //AlterTableDropForeignKey
	case 8:
		tp = "MODIFY COLUMN" //AlterTableModifyColumn
	case 9:
		tp = "CHANGE COLUMN" //AlterTableChangeColumn
	case 10:
		tp = "RENAME TO" //AlterTableRenameTable
	case 11:
		tp = "ALTER COLUMN" //AlterTableAlterColumn
	case 12:
		tp = "LOCK" //AlterTableLock
	case 13:
		tp = "ALGORITHM" //AlterTableAlgorithm
	}
	return tp
}

// Accept implements Node Accept interface.
func (n *AlterTableSpec) Accept(v Visitor) (Node, bool) {
	newNode, skipChildren := v.Enter(n)
	if skipChildren {
		return v.Leave(newNode)
	}
	n = newNode.(*AlterTableSpec)
	if n.Constraint != nil {
		node, ok := n.Constraint.Accept(v)
		if !ok {
			return n, false
		}
		n.Constraint = node.(*Constraint)
	}
	if n.NewTable != nil {
		node, ok := n.NewTable.Accept(v)
		if !ok {
			return n, false
		}
		n.NewTable = node.(*TableName)
	}
	for _, col := range n.NewColumns {
		node, ok := col.Accept(v)
		if !ok {
			return n, false
		}
		col = node.(*ColumnDef)
	}
	if n.OldColumnName != nil {
		node, ok := n.OldColumnName.Accept(v)
		if !ok {
			return n, false
		}
		n.OldColumnName = node.(*ColumnName)
	}
	if n.Position != nil {
		node, ok := n.Position.Accept(v)
		if !ok {
			return n, false
		}
		n.Position = node.(*ColumnPosition)
	}
	return v.Leave(n)
}

// AlterTableStmt is a statement to change the structure of a table.
// See https://dev.mysql.com/doc/refman/5.7/en/alter-table.html
type AlterTableStmt struct {
	ddlNode

	Table *TableName
	Specs []*AlterTableSpec
}

func (n *AlterTableStmt) GetDataBase() string {
	return n.Table.GetDataBase()
}
func (n *AlterTableStmt) SetDataBase(text string) {
	n.Table.SetDataBase(text)
}

func (n *AlterTableStmt) GetTable() string {
  return n.Table.GetTable()
}
func (n *AlterTableStmt) SetTable(text string) {
  n.Table.SetTable(text)
}

func (n *AlterTableStmt) GetRename() bool {
	for _, value := range n.Specs {
		if value.Tp == 10 {
			return true
		}
	}
	return false
}

func (n *AlterTableStmt) GetNewDataBase() string {
  return n.Specs[0].NewTable.GetDataBase()
}
func (n *AlterTableStmt) GetNewTable() string {
  return n.Specs[0].NewTable.GetTable()
}

func (n *AlterTableStmt) GetAlterSplit() []string {
	var result []string
	table := n.Table.Text()
	for _, specs := range n.Specs {
		if specs.Text() != "" {
			result = append(result, "ALTER TABLE " + table + " " + specs.Text())
		}
	}
	return result
}

func (n *AlterTableStmt) GetAlterHeadType() string {
	return n.Specs[0].GetAlterType()
}

func (n *AlterTableStmt) GetAlterHeadNewName() string  {
	if len(n.Specs) >= 1 && len(n.Specs[0].NewColumns) >= 1 {
		return n.Specs[0].NewColumns[0].Name.Text()
	} else {
		return ""
	}
}

func (n *AlterTableStmt) Text() string {
	table := n.Table.Text()
	var tempSpecs []string
	for _, value := range n.Specs {
		tempSpecs = append(tempSpecs, value.Text())
	}
	specs := strings.Join(ListFilterTrim(tempSpecs), ",")

	str := []string{
		"ALTER", "TABLE",
		table,
		specs,
	}

	return strings.Join(ListFilterTrim(str), " ")
}

// Accept implements Node Accept interface.
func (n *AlterTableStmt) Accept(v Visitor) (Node, bool) {
	newNode, skipChildren := v.Enter(n)
	if skipChildren {
		return v.Leave(newNode)
	}
	n = newNode.(*AlterTableStmt)
	node, ok := n.Table.Accept(v)
	if !ok {
		return n, false
	}
	n.Table = node.(*TableName)
	for i, val := range n.Specs {
		node, ok = val.Accept(v)
		if !ok {
			return n, false
		}
		n.Specs[i] = node.(*AlterTableSpec)
	}
	return v.Leave(n)
}

/* ***********************************************************************************************************
 * TruncateTableStmt 
 */
// TruncateTableStmt is a statement to empty a table completely.
// See https://dev.mysql.com/doc/refman/5.7/en/truncate-table.html
type TruncateTableStmt struct {
	ddlNode

	OptTable string
	Table *TableName
}

func (n *TruncateTableStmt) GetDataBase() string {
	return n.Table.GetDataBase()
}
func (n *TruncateTableStmt) SetDataBase(text string) {
	n.Table.SetDataBase(text)
}

func (n *TruncateTableStmt) GetTable() string {
	return n.Table.Name.O
}
func (n *TruncateTableStmt) SetTable(text string) {
	n.Table.Name.O = text
}

func (n *TruncateTableStmt) Text() string {
  optTable := n.OptTable //这个还未解析
  tableName := n.Table.Text()

  str := []string{
  	"TRUNCATE",
  	optTable,
  	tableName,
  }

	return strings.Join(ListFilterTrim(str), " ")
}

// Accept implements Node Accept interface.
func (n *TruncateTableStmt) Accept(v Visitor) (Node, bool) {
	newNode, skipChildren := v.Enter(n)
	if skipChildren {
		return v.Leave(newNode)
	}
	n = newNode.(*TruncateTableStmt)
	node, ok := n.Table.Accept(v)
	if !ok {
		return n, false
	}
	n.Table = node.(*TableName)
	return v.Leave(n)
}

/* ***********************************************************************************************************
 * Partition
 */
// PartitionDefinition defines a single partition.
type PartitionDefinition struct {
	Name     model.CIStr
	LessThan []ExprNode
	MaxValue bool
	Comment  string
}

func (n *PartitionDefinition) Text() string {
	identifier := n.Name.String()
	partDefValuesOpt := ""
	partDefCommentOpt := n.Comment
	partDefStorageOpt := ""

	str := []string{
		"PARTITION",
		identifier,
		partDefValuesOpt,
		partDefCommentOpt,
		partDefStorageOpt,
	}

	return strings.Join(ListFilterTrim(str), " ")
}

// PartitionOptions specifies the partition options.
type PartitionOptions struct {
	Tp          model.PartitionType
	Expr        ExprNode
	ColumnNames []*ColumnName
	Definitions []*PartitionDefinition
	Num         uint64
}

// 外层判空处理
func (n *PartitionOptions) Text() string {
	tp := n.Tp.String()
	expr := n.Expr.Text()
	var tempColumnNames []string
	for _, value := range n.ColumnNames {
		tempColumnNames = append(tempColumnNames, value.String())
	}
	columnNames := strings.Join(ListFilterTrim(tempColumnNames), ",")
	var tempDefinitions []string
	for _, value := range n.Definitions {
		tempDefinitions = append(tempDefinitions, value.Text())
	}
	definitions := strings.Join(ListFilterTrim(tempDefinitions), ",")
	num := strconv.FormatUint(n.Num, 10)

	str := []string{
		tp,
		expr,
		columnNames,
		definitions,
		num,
	}

	return strings.Join(ListFilterTrim(str), " ")
}
