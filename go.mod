module gitlab.com/feloxx/cdpparser

require (
	github.com/cznic/golex v0.0.0-20181122101858-9c343928389c // indirect
	github.com/cznic/mathutil v0.0.0-20181021201202-eba54fb065b7
	github.com/cznic/parser v0.0.0-20160622100904-31edd927e5b1
	github.com/cznic/sortutil v0.0.0-20150617083342-4c7342852e65
	github.com/cznic/strutil v0.0.0-20171016134553-529a34b1c186
	github.com/cznic/y v0.0.0-20170802143616-045f81c6662a
	github.com/go-sql-driver/mysql v0.0.0-20170715192408-3955978caca4
	github.com/juju/errors v0.0.0-20181118221551-089d3ea4e4d5
	github.com/juju/loggo v0.0.0-20180524022052-584905176618 // indirect
	github.com/juju/testing v0.0.0-20180920084828-472a3e8b2073 // indirect
	github.com/pingcap/check v0.0.0-20171206051426-1c287c953996
	github.com/pingcap/errors v0.11.0
	github.com/pingcap/tidb v0.0.0-20181203021530-741adcee43e2
	github.com/sirupsen/logrus v1.2.0
	golang.org/x/text v0.3.0
	gopkg.in/mgo.v2 v2.0.0-20180705113604-9856a29383ce // indirect
)
