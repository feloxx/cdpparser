package main

import (
	"C"
	"fmt"
	"gitlab.com/feloxx/cdpparser"
	"gitlab.com/feloxx/cdpparser/ast"
	"strings"
)

//全局的解析器对象,不用声明多了
var p *parser.Parser

func init() {
	p = parser.New()
}

func listDistinct(arr []string) (newArr []string) {
	newArr = make([]string, 0)
	for i := 0; i < len(arr); i++ {
		repeat := false
		for j := i + 1; j < len(arr); j++ {
			if arr[i] == arr[j] {
				repeat = true
				break
			}
		}
		if !repeat {
			newArr = append(newArr, arr[i])
		}
	}
	return
}

//export GetHello
func GetHello(msg *C.char) *C.char {

	a := C.GoString(msg)

	fmt.Println(a)

	return C.CString("hello : " + a)
}

//export GetCreateTableAllColumn
func GetCreateTableAllColumn(sql *C.char) *C.char {
	strSql := C.GoString(sql)

	stmts, err := p.ParseOneStmt(strSql, "", "")
	if err != nil {
		fmt.Println(err)
		return C.CString("传入的SQL语句有问题")
	}
	var cols []string
	switch n := stmts.(type) {
	case *ast.CreateTableStmt:
		for _, col := range n.Cols {
			cols = append(cols, col.Text())
		}
	}

	return C.CString(strings.Join(cols, "|"))
}

//export GetCreateTableColumn
func GetCreateTableColumn(sql *C.char, column *C.char) *C.char {
	strSql := C.GoString(sql)
	strColumn := C.GoString(column)

	listColumn := strings.Split(strColumn, ",")

	stmts, err := p.ParseOneStmt(strSql, "", "")
	if err != nil {
		fmt.Println(err)
		return C.CString("传入的SQL语句有问题")
	}
	var cols []string
	switch n := stmts.(type) {
	case *ast.CreateTableStmt:
		for _, col := range n.Cols {
			for _, c := range listColumn {
				if strings.Contains(col.Text(), c) {
					cols = append(cols, col.Text())
				}
			}
		}
	}

	return C.CString(strings.Join(listDistinct(cols), ","))
}

//export GetRelationJoin
func GetRelationJoin(sql *C.char) *C.char {
	strSql := C.GoString(sql)

	var join string

	stmts, err := p.ParseOneStmt(strSql, "", "")
	if err != nil {
		fmt.Println(err)
		fmt.Println("~start~")
		fmt.Println(strSql)
		fmt.Println("~end~")
		return C.CString("传入的SQL语句有问题")
	}

	switch n := stmts.(type) {
	case *ast.SelectStmt:
		n.SelectStartClean()
		result := n.GetSelectCleanData()
		if len(result.JoinRelation) <= 0 {
			return C.CString("Nil")
		}

		join = strings.Join(listDistinct(result.JoinRelation), "|")

	}

	return C.CString(join)
}

//export GetRelationFrom
func GetRelationFrom(sql *C.char) *C.char {
	strSql := C.GoString(sql)

	var from string

	stmts, err := p.ParseOneStmt(strSql, "", "")
	if err != nil {
		fmt.Println(err)
		fmt.Println("~start~")
		fmt.Println(strSql)
		fmt.Println("~end~")
		return C.CString("传入的SQL语句有问题")
	}

	switch n := stmts.(type) {
	case *ast.SelectStmt:
		n.SelectStartClean()
		result := n.GetSelectCleanData()
		if len(result.FromList) <= 0 {
			return C.CString("Nil")
		}

		from =  strings.Join(listDistinct(result.FromList), "|")

	}

	return C.CString(from)
}

//export GetAlterHeadType
func GetAlterHeadType(sql *C.char) *C.char {
	strSql := C.GoString(sql)

	var typ string

	stmts, err := p.ParseOneStmt(strSql, "", "")
	if err != nil {
		fmt.Println(err)
		fmt.Println("~start~")
		fmt.Println(strSql)
		fmt.Println("~end~")
		return C.CString("传入的SQL语句有问题")
	}

	switch n := stmts.(type) {
	case *ast.AlterTableStmt:
		typ = strings.ToLower(n.GetAlterHeadType())
	}

	return C.CString(typ)
}

//export GetAlterDatabase
func GetAlterDatabase(sql *C.char) *C.char {
	strSql := C.GoString(sql)

	var db string

	stmts, err := p.ParseOneStmt(strSql, "", "")
	if err != nil {
		fmt.Println(err)
		fmt.Println("~start~")
		fmt.Println(strSql)
		fmt.Println("~end~")
		return C.CString("传入的SQL语句有问题")
	}

	switch n := stmts.(type) {
	case *ast.AlterTableStmt:
		db = n.GetDataBase()
	}

	return C.CString(db)
}

//export GetAlterTable
func GetAlterTable(sql *C.char) *C.char {
	strSql := C.GoString(sql)

	var table string

	stmts, err := p.ParseOneStmt(strSql, "", "")
	if err != nil {
		fmt.Println(err)
		fmt.Println("~start~")
		fmt.Println(strSql)
		fmt.Println("~end~")
		return C.CString("传入的SQL语句有问题")
	}

	switch n := stmts.(type) {
	case *ast.AlterTableStmt:
		table = n.GetTable()
	}

	return C.CString(table)
}

//export GetAlterHeadNewName
func GetAlterHeadNewName(sql *C.char) *C.char {
	strSql := C.GoString(sql)

	var name string

	stmts, err := p.ParseOneStmt(strSql, "", "")
	if err != nil {
		fmt.Println(err)
		fmt.Println("~start~")
		fmt.Println(strSql)
		fmt.Println("~end~")
		return C.CString("传入的SQL语句有问题")
	}

	switch n := stmts.(type) {
	case *ast.AlterTableStmt:
		name = n.GetAlterHeadNewName()
	}

	return C.CString(name)
}

func main() {

}
