package main

/*================================================================================*\
 时间: 2018-09-28
 作者: 陈大炮
 内容: 解析sql语句,生成ast语法树,修改ast语法树中某个node,再根据修改后的ast生成新的sql
\*================================================================================*/

/* ***********************************************************************************************************
* 引包部分
*/
import (
	"fmt"
	"gitlab.com/feloxx/cdpparser"
	"gitlab.com/feloxx/cdpparser/ast"
	"gitlab.com/feloxx/cdpparser/testdata"
)

/* ***********************************************************************************************************
* ast 的访问者模式, 这里就简单的把每条语句的库名,表名修改一下
*/
type astVisitor struct {
	nodeList []ast.Node
}

func (v *astVisitor) Enter(in ast.Node) (out ast.Node, skipChildren bool) {

	//switch in.(type) {
	////case *ast.CreateTableStmt:
	////	fmt.Printf("原始语句 = %v\n", in)
	//case *ast.TableName:
	//	tables := in.(*ast.TableName)
	//	tables.SetText("大炮的表")
	//	//tables.Schema.O = "大炮的库"
	//	//tables.Name.O = "大炮的表"
	//}
	//
	//v.nodeList = append(v.nodeList, in)

	fmt.Printf("%#v\n",in)
	return in, false
}

func (v *astVisitor) Leave(in ast.Node) (out ast.Node, ok bool) {
	return in, true
}

/* ***********************************************************************************************************
* 重写语句
* 直接返回 ddl的重写后的字符串
* RewriteCreateDatabaseStmt
* RewriteDropDatabaseStmt
* RewriteCreateTableStmt
* RewriteDropTableStmt
* RewriteTruncateTableStmt
* RewriteRenameTableStmt
* RewriteAlterTableStmt
* RewriteCreateIndexStmt
* RewriteDropIndexStmt
*/
//func DdlRewrite(node ast.StmtNode) (db string, table string, sqlType string, isChanged bool, isDrop bool, newInfo []string, typeChange [][2]string){
//func DdlRewrite(node ast.StmtNode) (sql []string) {
func DDLRewrite(node ast.StmtNode, changeDataBase string, changeTable string) []string {
	var sql []string
	switch node {
	//case node.(*ast.SelectStmt):
	//	selectStmt := node.(*ast.SelectStmt)
	//	sql = selectStmt.Text()
	//
	//case node.(*ast.CreateDatabaseStmt):
	//	createDatabase := node.(*ast.CreateDatabaseStmt)
	//	createDatabase.SetText("大炮修改")
	//
	//	sql += createDatabase.Text()
	//
	//case node.(*ast.DropDatabaseStmt):
	//	dropDatabase := node.(*ast.DropDatabaseStmt)
	//
	//	db = dropDatabase.Name
	//	sql = dropDatabase.Text()
	//
	//case node.(*ast.CreateTableStmt):
	//	createTable := node.(*ast.CreateTableStmt)
	//	//createTable.Table.SetText("大炮修改")
	//
	//	sql = createTable.Text()
	//	db = createTable.GetDataBase()
	//	table = createTable.GetTable()
	//
	//case node.(*ast.DropTableStmt):
	//	dropTable := node.(*ast.DropTableStmt)
	//
	//	db = dropTable.GetDataBase()
	//	table = dropTable.GetTable()
	//	sql += dropTable.Text()
	//
	//case node.(*ast.RenameTableStmt):
	//	renameTableStmt := node.(*ast.RenameTableStmt)
	//	//renameTableStmt.OldTable.SetText("大炮修改旧表")
	//	//renameTableStmt.NewTable.SetText("大炮修改新表")
	//
	//	sql += renameTableStmt.Text()
	//
	//case node.(*ast.CreateViewStmt):
	//	createView := node.(*ast.CreateViewStmt)
	//	createView.SetText("大炮修改视图名")
	//
	//	sql += createView.Text()
	//
	//case node.(*ast.CreateIndexStmt):
	//	createIndex := node.(*ast.CreateIndexStmt)
	//
	//	sql += createIndex.Text()
	//
	//case node.(*ast.DropIndexStmt):
	//	dropIndex := node.(*ast.DropIndexStmt)
	//
	//	sql += dropIndex.Text()

	case node.(*ast.AlterTableStmt):
		alterTable := node.(*ast.AlterTableStmt)

		alterTable.SetDataBase(changeDataBase)
		alterTable.SetTable(changeTable)

		alterList := alterTable.GetAlterSplit()

		sql = append(sql, alterList...)

		//db = alterTable.GetDataBase()
		//table = alterTable.GetTable()
		//if alterTable.GetRename() {
		//	isChanged = true
		//	isDrop = true
		//}
		//
		//tdb, ttb := "", ""
		//if alterTable.Specs[0].NewTable != nil {
		//	tdb = alterTable.GetNewTable()
		//	ttb = alterTable.GetNewDataBase()
		//}
		//newInfo = []string{tdb, ttb}
		//
		//for _, Specs := range alterTable.Specs {
		//	for _, cols := range Specs.NewColumns {
		//		typeChange = append(typeChange, [2]string{cols.GetName(), cols.GetTp()})
		//	}
		//}

		sql = alterTable.GetAlterSplit()

	//case node.(*ast.TruncateTableStmt):
	//	truncateTable := node.(*ast.TruncateTableStmt)
	//
	//	sql += truncateTable.Text()
	}

	return sql
}

/* ***********************************************************************************************************
* main
*/
func main() {

	//value := "ALTER TABLE asset_advance_strategy_record ADD COLUMN advance_type varchar(64) NOT NULL DEFAULT '' COMMENT '垫付类型'"
	tidbParser := parser.New()

	for _, value := range testdata.GetAlterData() {
		//stmts1, err := tidbParser.Parse(value, "", "")
		stmts, err := tidbParser.ParseOneStmt(value, "", "")

		if err != nil {
			fmt.Printf("parse error:\n%v\n", err)
			return
		}

		sql := DDLRewrite(stmts, "db","table")

		//db, table, sqlType, isChanged, isDrop, newInfo, typeChange := DdlRewrite(stmts)

		//v := astVisitor{}
		//	for _, stmtNode := range stmts1 {
		//		stmtNode.Accept(&v)
		//	}

		//fmt.Println("originSql: \n" + value)
		//fmt.Println("sql: \n" + sql)

		for _, value := range sql {
			fmt.Println(value)
		}

		//fmt.Println(db)
		//fmt.Println(table)
		//fmt.Println(sqlType)
		//fmt.Println(isChanged)
		//fmt.Println(isDrop)
		//fmt.Println(newInfo)
		//fmt.Println(typeChange)
		//fmt.Println("db: " + db)
		//fmt.Println("table: " + table)
	}
}

